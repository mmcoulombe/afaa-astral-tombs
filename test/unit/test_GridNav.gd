extends 'res://addons/gut/test.gd'

const GridNav = preload('res://scripts/navigation/GridNav.gd')
const LevelMeta = preload('res://scripts/LevelMeta.gd')

var level_info: LevelMeta = LevelMeta.new();

var grid_nav: GridNav

func before_all():
    level_info.nb_floor = 1
    level_info.size = Vector2(4, 4)
    level_info.player_cell = Vector2(2, 3)
    level_info.level_data = [
        [
            2, 1, 2 ,0,
            1, 6, 1, 2,
            2, 1, 2, 0,
            0, 2, 0, 0
        ]
    ]

func before_each():
    grid_nav = GridNav.new()

func test_graph_creation():
    grid_nav.create_nav_grid(level_info)
    var cell = Array(grid_nav.generate_nav_graph(Vector2(2, 3)))
    assert_eq(cell.size(), 8)
    assert_has(cell, Vector2(0, 3))

    assert_has(cell, Vector2(1, 2))
    assert_has(cell, Vector2(1, 3))

    assert_has(cell, Vector2(2, 1))
    assert_has(cell, Vector2(2, 2))
    assert_has(cell, Vector2(2, 3))

    assert_has(cell, Vector2(3, 2))
    assert_has(cell, Vector2(3, 3))

func test_another_position():
    grid_nav.create_nav_grid(level_info)
    var cell = Array(grid_nav.generate_nav_graph(Vector2(1, 1)))
    assert_eq(cell.size(), 11)

    assert_has(cell, Vector2(0, 0))
    assert_has(cell, Vector2(0, 1))
    assert_has(cell, Vector2(0, 2))

    assert_has(cell, Vector2(1, 0))
    assert_has(cell, Vector2(1, 1))
    assert_has(cell, Vector2(1, 2))
    assert_has(cell, Vector2(1, 3))

    assert_has(cell, Vector2(2, 0))
    assert_has(cell, Vector2(2, 1))
    assert_has(cell, Vector2(2, 2))

    assert_has(cell, Vector2(3, 1))

