/// https://www.shadertoy.com/view/4dfBWn
shader_type canvas_item;

const float AA_FALLOFF = 1.2;
const float CURVE_WIDTH = 5.0;
const float FUNC_SAMPLE_STEP = 0.05;
const float SCOPE_RATE = 0.5;

uniform vec2 screen_size;

float sinc(float value)
{
    return value == 0.0 ? 1.0 : sin(value) / value;    
}

float triIsolate(float value)
{
    return abs(-1.0 + fract(clamp(value, -0.5, 0.5)) * 2.0);
}

float heartbeat(float value)
{
    float prebeat = -sinc((value - 0.4) / 40.0) * 0.6 * triIsolate((value - 0.4) * 1.0);   
    float mainbeat = (sinc((value - 0.5) * 60.0)) * 1.2 / triIsolate((value - 0.5) * 0.7);
    float postbeat = sinc((value - 0.85) * 15.0) * 0.5 * triIsolate((value - 0.85) * 0.6);
    
    return (prebeat + mainbeat + postbeat) * triIsolate((value - 0.625) * 0.8);
}

float func(float value)
{
    return 0.5 * heartbeat(mod((value + 0.25), 1.3));    
}

float aaStep(float a, float b, float x)
{
    x = clamp(x , a, b);
    return (x - a) / (b - a);    
}

void blend(inout vec4 baseCol, vec4 color, float alpha)
{
    baseCol = vec4(mix(baseCol.rgb, color.rgb, alpha * color.a), 1.0);    
}

void drawFunc(inout vec4 baseCol, vec2 xy, vec4 curveCol, float pp)
{
    // samples the function around x neighborhood to get distance to curve
    float hlw = CURVE_WIDTH * pp * 0.5;
    
    // cover line width and aa
    float left = xy.x - hlw - pp * AA_FALLOFF;
    float right = xy.x + hlw + pp * AA_FALLOFF;
    float closest = 100000.0;
    for (float x = left; x <= right; x+= pp * FUNC_SAMPLE_STEP)
    {
        vec2 diff = vec2(x, func(x)) - xy;
        float dSqr = dot(diff, diff);
        closest = min(dSqr, closest);
    }
    
	float c = 1.0 - aaStep(0.0, hlw + pp * AA_FALLOFF, sqrt(closest));
	blend(baseCol, curveCol, c * c * c);
}

float findMagnitude(float range)
{
    float l10 = log(range) / log(10.0);
    return pow(10.0, floor(l10));     
}

void fragment()
{
    vec2 uv = UV;
    // graph setup
    float aspect = screen_size.x / screen_size.y;
    float z = 0.0;
    
    float graphRange = 0.4  + pow(1.2, z * z * z);
    vec2 graphSize = vec2(aspect * graphRange, graphRange);
    vec2 graphCenter = vec2(0.5, 0.1);
    vec2 graphPos = graphCenter - graphSize * 0.5;
    
    vec2 xy = graphPos + uv * graphSize;
    float pp = graphSize.y / screen_size.y;
    
    
    vec4 col = vec4(vec3(0), 1.0);
    float rate = SCOPE_RATE;
    
    float pulse = fract(TIME * rate) * 4.0 - 1.5;
    float fade = pulse - xy.x;
    if (fade < 0.0) fade += 4.0;
    fade *= 0.25;
    fade = clamp(fade / rate, 0.0, 1.0);
    fade = 1.0 - fade;
    fade = fade * fade * fade;
    fade *= step(-1.5, xy.x) * step(xy.x, 2.5);
    vec4 pulseColor = vec4(0.0, 1.0, 0.7, fade * 1.5);
    drawFunc(col, xy, pulseColor, pp);
    pulseColor.a = 1.5;
    
    COLOR = col;
}