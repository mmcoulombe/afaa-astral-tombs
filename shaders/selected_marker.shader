shader_type spatial;
render_mode unshaded;

uniform vec4 color: hint_color = vec4(1);
uniform sampler2D marker_texture: hint_albedo;

void fragment()
{
    vec2 base_uv = UV;
	vec4 albedo_tex = texture(marker_texture, base_uv);
    ALBEDO = albedo_tex.rgb * color.rgb;
    ALPHA = albedo_tex.a;
}