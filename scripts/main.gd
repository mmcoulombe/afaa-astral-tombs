extends Spatial

export(PackedScene) var selected_marker

onready var dungeon = $"Dungeon"
onready var player = $"Player"

var marker_instance
var move_maker_list = []
var selection_info = {
    'has_selection': false,
    'cell_position': Vector3(-1.0, -1.0, -1.0),
    'selection_grid': []
}

### --- internal hook --- ###
func _ready() -> void:
    _instanciate_selection_marker()
    _set_initial_character_position()

### --- private function --- ###
func _handle_cell_selected(global_cell: Vector3) -> void:
    marker_instance.translation = Vector3(global_cell.x, 2.01, global_cell.z)
    marker_instance.visible = true

func _handle_player_selected(player_cell: Vector3) -> void:
        print('player selected')
        _show_possible_path(player_cell)

func _instanciate_selection_marker() -> void:
    marker_instance = selected_marker.instance()
    marker_instance.visible = false
    marker_instance.colour = Color.white
    add_child(marker_instance)

func _show_possible_path(player_cell: Vector3) -> void:
    var cells = dungeon.get_available_cells(player_cell, 2)
    selection_info.has_selection = true
    selection_info.cell_position = player_cell
    selection_info.selection_grid = Array(cells)
    for i in range(len(cells)):
        var cell = cells[i]
        var global_pos = dungeon.map_to_word(Vector3(cell.x, 0, cell.y))
        var inst = selected_marker.instance();
        inst.translation = Vector3(global_pos.x, 2.01, global_pos.z)
        inst.visible = true
        inst.colour = Color.blue
        add_child(inst)
        move_maker_list.push_back(inst)

func _set_initial_character_position() -> void:
    player.translation = dungeon.get_player_initial_position();
    player.translation.y = 2.05;

func _clear_movement_list() -> void:
    for _i in range(len(move_maker_list)):
        var instance = move_maker_list.pop_back()
        instance.queue_free()
    marker_instance.colour = Color.white

### --- public function --- ###
func is_character_selected(character_pos: Vector3, selected_cell: Vector3) -> bool:
    return character_pos.x == selected_cell.x and character_pos.z == selected_cell.z

### --- signal hook --- ###
func _on_Dungeon_cell_clicked(event) -> void:
    var global_pos = event.world_position
    var cell_index = event.cell_index
    if not selection_info.has_selection:
        var player_cell = dungeon.world_to_map(player.translation)

        if is_character_selected(player_cell, cell_index):
            _handle_player_selected(player_cell)
        else:
            _clear_movement_list()
    else:
        if _can_move_to_position(cell_index):
            _move_to(global_pos)
        _clear_movement_list()
        selection_info.has_selection = false
    _handle_cell_selected(global_pos)

func _can_move_to_position(pos: Vector3) -> bool:
    return selection_info.cell_position != pos and selection_info.selection_grid.has(Vector2(pos.x, pos.z))

func _move_to(pos: Vector3) -> void:
    pos.y = player.translation.y
    print('move to' + str(pos))
    var path = dungeon.get_path_to_cell(player.translation, pos)
    player.move_to(path)
    yield(player, 'move_completed')
