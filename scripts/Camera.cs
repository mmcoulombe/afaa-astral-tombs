using Godot;
using System;



public class Camera : Godot.Camera
{
    [Export]
    private Int32 _castLength = 100;

    [Signal]
    delegate void ObjectSelected(Godot.Collections.Dictionary collider);

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

    public override void _Input(InputEvent @event)
    {
        if (@event is InputEventMouseButton mouseEvent)
        {
            if (mouseEvent.Pressed && mouseEvent.ButtonIndex == 1)
            {
                Vector3 from = this.ProjectRayOrigin(mouseEvent.Position);
                Vector3 to = from + this.ProjectRayNormal(mouseEvent.Position) * _castLength;
                PhysicsDirectSpaceState spaceState = GetWorld().DirectSpaceState;
                Godot.Collections.Dictionary result = spaceState.IntersectRay(from, to);
                if (result.Count > 0)
                {
                    GD.Print(result);
                    EmitSignal("ObjectSelected", result);
                }
            }
        }
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
