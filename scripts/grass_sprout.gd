extends MultiMeshInstance


export(Vector2) var max_distance = Vector2.ZERO
export(int) var max_rotation = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    for i in range(multimesh.instance_count):
        var pos = Transform()
        var tranlate = Vector3(rand_range(-max_distance.x, max_distance.x), 0, rand_range(-max_distance.y, max_distance.y))
        pos = pos.translated(tranlate)
        pos = pos.rotated(Vector3.UP, deg2rad(max_rotation))
        multimesh.set_instance_transform(i, pos);

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#    pass
