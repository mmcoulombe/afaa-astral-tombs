extends Spatial
class_name Dungeon

const LevelMeta = preload('res://scripts/level_meta.gd')
const GridNavigation = preload('res://scripts/navigation/grid_navigation.gd')

signal cell_clicked(event)
# signal cell_overed(global_pos)

export(Resource) var level_meta
export(bool) var can_pick = true

onready var grid: GridMap = $"GridMap"
onready var camera: Camera = $"../Camera"

var grid_nav
const ray_length = 1000

### --- game engine hook --- ###
func _ready() -> void:
    grid_nav = GridNavigation.new()
    if level_meta != null:
        # _parse_lvl_meta()
        grid_nav.create_nav_grid(level_meta)

func _input(event: InputEvent) -> void:
    if not can_pick:
        return

    if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
        var cell_index = get_cell_under_mouse(event.position)
        if cell_index != null:
            var cell_clicked_event = {
                'cell_index': cell_index,
                'world_position': map_to_word(cell_index)
            }
            emit_signal('cell_clicked', cell_clicked_event)

### --- private function --- ###
func _clamp_limit(value, min_bounds, max_bounds, offset):
    if value <= min_bounds:
        return value + offset
    elif value >= max_bounds:
        return value - offset
    return value

func _get_value_by_divider(value, origin, divider):
    return floor((value - origin) / divider)

func _parse_lvl_meta() -> void:
    var length = level_meta.size.x + level_meta.size.y
    for i in range(length):
        var info = level_meta.level_data[0][i]
        grid.set_cell_item(info.pos.x, info.pos.y, info.pos.z, info.item)

### --- public function --- ###
func get_cell_under_mouse(mouse_pos: Vector2):
    var from = camera.project_ray_origin(mouse_pos)
    var to = from + camera.project_ray_normal(mouse_pos) * ray_length

    var space_state = get_world().direct_space_state
    var result = space_state.intersect_ray(from, to)
    if not result.empty():
        return world_to_map(result.position)
    return null

func get_player_initial_position() -> Vector3:
    var cell = Vector3(level_meta.player_cell.x, 0, level_meta.player_cell.y);
    return map_to_word(cell)

func get_available_cells(root_cell_pos: Vector3, step_count: int) -> PoolVector3Array:
    var top_view_root_cell = Vector2(root_cell_pos.x, root_cell_pos.z)
    return grid_nav.generate_nav_graph(top_view_root_cell, step_count)

func get_path_to_cell(_start: Vector3, final: Vector3) -> PoolVector3Array:
    var lst = PoolVector3Array()
    lst.push_back(final)
    return lst

func is_under_cell(_cell: Vector3) -> bool:
    if (level_meta as LevelMeta).nbFloor == 0:
        return false
    return true

func map_to_word(cell: Vector3):
    var top_left_pos = Vector2(grid.translation.x, grid.translation.z) - level_meta.size
    var pos = Vector3.ZERO
    pos.x = top_left_pos.x + (cell.x * grid.cell_size.x) + grid.cell_size.x * 0.5
    pos.y = 0
    pos.z = top_left_pos.y + (cell.z * grid.cell_size.z) + grid.cell_size.z * 0.5
    return pos;

func world_to_map(pos: Vector3):
    var top_left_pos = Vector2(grid.translation.x, grid.translation.z) - level_meta.size
    var cell = Vector3.ZERO

    pos.x = _clamp_limit(pos.x, -level_meta.size.x, level_meta.size.x, 0.1)
    pos.y = _clamp_limit(pos.y, 0, level_meta.nb_floor * grid.cell_size.y, 0.1)
    pos.z = _clamp_limit(pos.z, -level_meta.size.y, level_meta.size.y, 0.1)

    cell.x = _get_value_by_divider(pos.x, top_left_pos.x, grid.cell_size.x)
    cell.y = 0 # int(floor(pos.y / grid.cell_size.y))
    cell.z = _get_value_by_divider(pos.z, top_left_pos.y, grid.cell_size.z)
    return cell
