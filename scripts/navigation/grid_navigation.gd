class_name GridNavigation

const LevelMeta = preload('res://scripts/level_meta.gd')

class NavNode:
    var visited: bool
    var weight: int
    var cell_position: Vector2
    var cell_index: int

    func _init(_cell_position: Vector2, _cell_index, _weight: int = 1) -> void:
        visited = false
        weight = _weight
        cell_position = _cell_position
        cell_index = _cell_index

class NavigationGridContainer:
    var size: Vector2
    var grid: Array

    func _init(_grid: Array, _size: Vector2) -> void:
        size = _size
        grid = _grid

    func get_cell_at(row: int, col: int) -> NavNode:
        row = int(clamp(row, 0, size.x - 1))
        col = int(clamp(col, 0, size.y - 1))
        return grid[col][row]

var grid: NavigationGridContainer

func get_grid() -> NavigationGridContainer:
    return grid

func create_nav_grid(data: LevelMeta) -> void:
    var result = []
    for f in range(data.nb_floor):
        var size = data.size
        var lvl = data.level_data[f]
        for y in range(size.y):
            var row = []
            for x in range(size.x):
                var index = y * size.x + x
                row.push_back(NavNode.new(Vector2(x, y), lvl[f][index], 1))
            result.push_back(row)

    grid = NavigationGridContainer.new(result, data.size)

func generate_nav_graph(position: Vector2, iteration: int = 2) -> PoolVector2Array:
    var cell = grid.grid[position.y][position.x]
    var result = _visit_cells(cell, iteration)
    _reset_visited()
    return result

func _visit_cells(node: NavNode, it: int) -> PoolVector2Array:
    var result = PoolVector2Array()
    node.visited = true
    var cell_pos = node.cell_position
    result.push_back(cell_pos)

    var left = grid.get_cell_at(cell_pos.x - 1, cell_pos.y)
    result.append_array(_check(left, it - 1))

    var right = grid.get_cell_at(cell_pos.x + 1, cell_pos.y)
    result.append_array(_check(right, it - 1))

    var top = grid.get_cell_at(cell_pos.x, cell_pos.y - 1)
    result.append_array(_check(top, it - 1))

    var bottom = grid.get_cell_at(cell_pos.x, cell_pos.y + 1)
    result.append_array(_check(bottom, it - 1))

    return result

func _reset_visited() -> void:
    for y in range(len(grid.grid)):
        for x in range(len(grid.grid[0])):
            grid.grid[y][x].visited = false

func _check(child: NavNode, it: int):
    if child != null and not child.visited and it >= 0 and child.cell_index != -1:
        return _visit_cells(child, it)
    return []
