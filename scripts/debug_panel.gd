extends Control


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

onready var lblFps = $"FPSLabel"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    pass # Replace with function body.

func _process(_delta: float) -> void:
    lblFps.text = "FPS: " + str(Engine.get_frames_per_second())

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#    pass
