extends Spatial

signal move_completed()

onready var tween = $Tween

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    pass # Replace with function body.

func move_to(lst_positions: PoolVector3Array, speed: float = 1.0) -> void:
    for i in range(len(lst_positions)):
        var pos = lst_positions[i]
        tween.interpolate_property(self, "translation", self.translation, pos, speed, Tween.TRANS_LINEAR)
        tween.start()
        yield(tween, 'tween_completed')
    emit_signal('move_completed')

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#    pass
