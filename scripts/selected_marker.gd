extends MeshInstance


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

var colour: Color = Color.white setget set_colour

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    get_surface_material(0).set_shader_param('color', colour)

func set_colour(value: Color) -> void:
    colour = value
    if is_inside_tree():
        get_surface_material(0).set_shader_param('color', colour)
