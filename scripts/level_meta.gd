extends Resource
class_name LevelMeta

export(Vector2) var size = Vector2(6, 4)
export(Vector2) var player_cell = Vector2(1, 0)
export(int) var nb_floor = 1
export(Array, Array) var level_data = [
    [
        [
            -1, 0, 0, 0, 0, -1,
            -1, 0, 0, 0, 0, 0,
            -1, 0, 0, 0, 0, 0,
            -1, 0, 0, 0, 0, -1,
        ],
    ]
]
